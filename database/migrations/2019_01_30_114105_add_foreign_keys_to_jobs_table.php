<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToJobsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('jobs', function(Blueprint $table)
		{
			$table->foreign('finalapproval', 'final_stage')->references('id')->on('stages')->onUpdate('NO ACTION')->onDelete('CASCADE');
			$table->foreign('gcfo', 'gcfo_stage')->references('id')->on('stages')->onUpdate('NO ACTION')->onDelete('CASCADE');
			$table->foreign('hod', 'hod_stage')->references('id')->on('stages')->onUpdate('NO ACTION')->onDelete('CASCADE');
			$table->foreign('hr', 'hr_stage')->references('id')->on('stages')->onUpdate('NO ACTION')->onDelete('CASCADE');
			$table->foreign('designation_id')->references('id')->on('designations')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('skill_id')->references('id')->on('skills')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('stage_id')->references('id')->on('stages')->onUpdate('SET NULL')->onDelete('CASCADE');
			$table->foreign('user_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('md', 'md_stage')->references('id')->on('stages')->onUpdate('NO ACTION')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('jobs', function(Blueprint $table)
		{
			$table->dropForeign('final_stage');
			$table->dropForeign('gcfo_stage');
			$table->dropForeign('hod_stage');
			$table->dropForeign('hr_stage');
			$table->dropForeign('jobs_designation_id_foreign');
			$table->dropForeign('jobs_skill_id_foreign');
			$table->dropForeign('jobs_stage_id_foreign');
			$table->dropForeign('jobs_user_id_foreign');
			$table->dropForeign('md_stage');
		});
	}

}
