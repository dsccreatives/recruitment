<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJobsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('jobs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('designation_id')->unsigned()->nullable()->index('jobs_designation_id_foreign');
			$table->string('experience', 191);
			$table->enum('job_type', array('Contractual','Part Time.','Full Time'));
			$table->string('age', 191);
			$table->string('job_location', 191);
			$table->string('salary_range', 191);
			$table->text('short_description', 65535);
			$table->string('post_date');
			$table->string('ldate');
			$table->string('cdate');
			$table->enum('status', array('Closed','Open','Drafted'));
			$table->string('jreference', 199)->nullable();
			$table->text('description', 65535);
			$table->integer('no_position');
			$table->integer('user_id')->unsigned()->index('jobs_user_id_foreign');
			$table->timestamps();
			$table->integer('skill_id')->unsigned()->nullable()->index('jobs_skill_id_foreign');
			$table->integer('stage_id')->unsigned()->nullable()->index('stage');
			$table->integer('hod')->unsigned()->nullable();
			$table->integer('md')->unsigned()->nullable()->index('md_stage');
			$table->integer('hr')->unsigned()->nullable()->index('hr_stage');
			$table->integer('gcfo')->unsigned()->nullable()->index('gcfo_stage');
			$table->integer('finalapproval')->unsigned()->nullable()->index('finalapproval');
			$table->index(['hod','md','hr','gcfo'], 'hod');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('jobs');
	}

}
