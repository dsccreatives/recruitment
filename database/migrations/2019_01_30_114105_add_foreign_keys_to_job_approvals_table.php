<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToJobApprovalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('job_approvals', function(Blueprint $table)
		{
			$table->foreign('job_id')->references('id')->on('jobs')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('stage_id')->references('id')->on('stages')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('user_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('job_approvals', function(Blueprint $table)
		{
			$table->dropForeign('job_approvals_job_id_foreign');
			$table->dropForeign('job_approvals_stage_id_foreign');
			$table->dropForeign('job_approvals_user_id_foreign');
		});
	}

}
