<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateShortlistingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('shortlistings', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('applicant_id')->unsigned()->nullable()->index('shortlistings_applicant_id_foreign');
			$table->integer('user_id')->unsigned()->nullable()->index('shortlistings_user_id_foreign');
			$table->integer('job_id')->unsigned()->nullable()->index('shortlistings_job_id_foreign');
			$table->string('reason', 191)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('shortlistings');
	}

}
