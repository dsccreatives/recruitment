<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('email', 191)->unique();
			$table->string('username', 191)->nullable()->index();
			$table->string('password', 191);
			$table->string('first_name', 191)->nullable();
			$table->string('last_name', 191)->nullable();
			$table->string('phone', 191)->nullable();
			$table->string('avatar', 191)->nullable();
			$table->string('address', 191)->nullable();
			$table->integer('country_id')->unsigned()->nullable()->index('users_country_id_foreign');
			$table->integer('role_id')->unsigned()->index('users_role_id_foreign');
			$table->date('birthday')->nullable();
			$table->dateTime('last_login')->nullable();
			$table->string('confirmation_token', 60)->nullable();
			$table->string('status', 20)->index();
			$table->integer('two_factor_country_code')->nullable();
			$table->integer('two_factor_phone')->nullable();
			$table->text('two_factor_options', 65535)->nullable();
			$table->string('remember_token', 100)->nullable();
			$table->timestamps();
			$table->string('resume', 191)->nullable();
			$table->string('coverletter', 191)->nullable();
			$table->string('certs', 191)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
