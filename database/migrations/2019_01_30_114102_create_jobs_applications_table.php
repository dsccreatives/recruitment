<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJobsApplicationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('jobs_applications', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned()->nullable()->index('jobs_applications_user_id_foreign');
			$table->integer('job_id')->unsigned()->nullable()->index('jobs_applications_job_id_foreign');
			$table->string('status', 191);
			$table->string('levels', 191)->nullable();
			$table->text('resume')->nullable();
			$table->text('coverletter')->nullable();
			$table->integer('approvedby_id')->unsigned()->nullable()->index('jobs_applications_approvedby_id_foreign');
			$table->dateTime('dateapproved')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('jobs_applications');
	}

}
