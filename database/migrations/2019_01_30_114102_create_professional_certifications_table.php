<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProfessionalCertificationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('professional_certifications', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('certification', 191);
			$table->date('dateacquired');
			$table->date('expirationdate')->nullable();
			$table->integer('user_id')->unsigned()->nullable()->index('professional_certifications_user_id_foreign');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('professional_certifications');
	}

}
