<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWorkExperiencesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('work_experiences', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('employer', 191);
			$table->integer('user_id')->unsigned()->nullable()->index('work_experiences_user_id_foreign');
			$table->integer('designation_id')->unsigned()->nullable()->index('work_experiences_designation_id_foreign');
			$table->integer('country_id')->unsigned()->nullable()->index('work_experiences_country_id_foreign');
			$table->string('startdate', 191);
			$table->string('enddate', 191)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('work_experiences');
	}

}
