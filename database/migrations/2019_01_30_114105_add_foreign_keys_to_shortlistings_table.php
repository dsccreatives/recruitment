<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToShortlistingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('shortlistings', function(Blueprint $table)
		{
			$table->foreign('applicant_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('job_id')->references('id')->on('jobs')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('user_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('shortlistings', function(Blueprint $table)
		{
			$table->dropForeign('shortlistings_applicant_id_foreign');
			$table->dropForeign('shortlistings_job_id_foreign');
			$table->dropForeign('shortlistings_user_id_foreign');
		});
	}

}
