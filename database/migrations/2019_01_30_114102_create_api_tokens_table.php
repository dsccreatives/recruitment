<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateApiTokensTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('api_tokens', function(Blueprint $table)
		{
			$table->string('id', 40)->primary();
			$table->integer('user_id')->unsigned()->index('api_tokens_user_id_foreign');
			$table->string('ip_address', 45)->nullable();
			$table->text('user_agent', 65535)->nullable();
			$table->timestamps();
			$table->dateTime('expires_at')->nullable();
			$table->index(['id','expires_at']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('api_tokens');
	}

}
