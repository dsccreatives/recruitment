<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEducationBackgroundsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('education_backgrounds', function(Blueprint $table)
		{
			$table->foreign('areastudied_id')->references('id')->on('area_studies')->onUpdate('NO ACTION')->onDelete('CASCADE');
			$table->foreign('certificate_id')->references('id')->on('degree_certificates')->onUpdate('NO ACTION')->onDelete('CASCADE');
			$table->foreign('country_id')->references('id')->on('countries')->onUpdate('NO ACTION')->onDelete('CASCADE');
			$table->foreign('user_id')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('education_backgrounds', function(Blueprint $table)
		{
			$table->dropForeign('education_backgrounds_areastudied_id_foreign');
			$table->dropForeign('education_backgrounds_certificate_id_foreign');
			$table->dropForeign('education_backgrounds_country_id_foreign');
			$table->dropForeign('education_backgrounds_user_id_foreign');
		});
	}

}
