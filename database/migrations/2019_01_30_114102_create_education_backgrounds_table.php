<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEducationBackgroundsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('education_backgrounds', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('institution', 191);
			$table->integer('user_id')->unsigned()->nullable()->index('education_backgrounds_user_id_foreign');
			$table->integer('areastudied_id')->unsigned()->nullable()->index('education_backgrounds_areastudied_id_foreign');
			$table->integer('certificate_id')->unsigned()->nullable()->index('education_backgrounds_certificate_id_foreign');
			$table->integer('country_id')->unsigned()->nullable()->index('education_backgrounds_country_id_foreign');
			$table->date('startdate');
			$table->date('enddate')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('education_backgrounds');
	}

}
