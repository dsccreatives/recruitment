<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJobApprovalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('job_approvals', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned()->nullable()->index('job_approvals_user_id_foreign');
			$table->integer('job_id')->unsigned()->nullable()->index('job_approvals_job_id_foreign');
			$table->integer('stage_id')->unsigned()->nullable()->index('job_approvals_stage_id_foreign');
			$table->string('reason', 191)->nullable();
			$table->string('status', 191)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('job_approvals');
	}

}
