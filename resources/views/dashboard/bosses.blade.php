@extends('layouts.bosses')

@section('page-title', trans('app.dashboard'))
@section('page-heading', trans('app.dashboard'))

@section('breadcrumbs')
    <li class="breadcrumb-item active">
        @lang('app.dashboard')
    </li>
@stop

@section('content')

<div class="row">
        <div class="element-wrapper col-12">
                <h6 class="element-header">Job Requests Approved & Rejected Statistics</h6>
                <div class="row">
                    
                <div class="col">
                        <div class="card text-white bg-primary mb-3 no-b card-body">
                            <h4>HOD</h4>
                            <span>{{ count($hodapproved) }}</span>
                        </div>
                    </div>
                    <div class="col">
                            <div class="card text-white bg-info mb-3 no-b card-body">
                                <h4>HR</h4>
                                <span>{{ count($hrapproved) }}</span>
                            </div>
                    </div>
                    <div class="col">
                            <div class="card text-white bg-success mb-3 no-b card-body">
                                <h4>GCFO</h4>
                                <span>{{ count($gcfoapproved) }}</span>
                            </div>
                    </div>
                    <div class="col">
                            <div class="card text-white bg-warning mb-3 no-b card-body">
                                <h4>MD</h4>
                                <span>{{ count($mdapproved) }}</span>
                            </div>
                    </div>
                    <div class="col">
                            <div class="card text-white bg-secondary mb-3 no-b card-body">
                                <h4>Final</h4>
                                <span>{{ count($mdapproved) }}</span>
                            </div>
                    </div>
                    <div class="col">
                            <div class="card text-white bg-danger mb-3 no-b card-body">
                                <h4>Rejected</h4>
                                <span>{{ count($rejectedjobs) }}</span>
                            </div>
                    </div>
                </div>
</div>
</div>
@permission('manage.hodjobs')
<div class="row">
    <div class="col-12">
            
                <div class="alert alert-primary" role="alert">
                       {{ $greetings. ', '. auth()->user()->present()->nameOrEmail }}
                       <div id="time" style="float:right"></div>
                      </div>
                      
    </div>
</div>
<div class="element-wrapper">
        <h6 class="element-header">Job Requests</h6>

<div class="row">
    <div class="element-box-tp col-12">
           
        <div class="table-responsive">
            <table class="table table-padded">
                <thead>
                    <tr>
                        <th>Position</th>
                        <th>Job Location</th>
                        <th>Posted Date</th>
                        <th class="text-center">Close Date</th>
                        <th class="text-right">Status</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($vacanciesnotapproved as $item)
                    <tr>
                        <td class="nowrap">
                            <span class="status-pill smaller green"></span>
                                <span>{{ $item->designations->name }}</span>
                            </td>
                            <td>
                                <span>{{ $item->job_location }}</span>
                                {{-- <span class="smaller lighter">1:52am</span> --}}
                            </td>
                            <td class="cell-with-media">
                                <img alt="" src="img/company1.png" style="height: 25px;">
                                <span>{{ $item->post_date }}</span>
                            </td>
                            <td class="text-right bolder nowrap">
                                    <span class="text-success">{{ $item->cdate }}</span>
                                </td>
                            <td class="text-center">
                               
                                @if ($item->status == 'Open')
                                                    <span class="badge badge-warning">{{ $item->status }}</span>
                                            @else
                                            <span class="badge badge-primary">{{ $item->status }}</span>
                                            @endif
                            </td>
                            
                        </tr>
                        @endforeach
                    </tbody>
                </table>
        </div>
    </div>
</div>
</div>
<div class="element-wrapper approved">
        <h6 class="element-header">Approved Job Requests</h6>

<div class="row">
    <div class="element-box-tp col-12">
           
        <div class="table-responsive">
            <table class="table table-padded">
                <thead>
                    <tr>
                        <th>Position</th>
                        <th>Job Location</th>
                        <th>Posted Date</th>
                        <th class="text-center">Close Date</th>
                        <th class="text-right">Status</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($hodapproved as $item)
                    <tr>
                        <td class="nowrap">
                            <span class="status-pill smaller green"></span>
                                <span>{{ $item->designations->name }}</span>
                            </td>
                            <td>
                                <span>{{ $item->job_location }}</span>
                                {{-- <span class="smaller lighter">1:52am</span> --}}
                            </td>
                            <td class="cell-with-media">
                                <img alt="" src="img/company1.png" style="height: 25px;">
                                <span>{{ $item->post_date }}</span>
                            </td>
                            <td class="text-right bolder nowrap">
                                    <span class="text-success">{{ $item->cdate }}</span>
                                </td>
                            <td class="text-center">
                               
                                @if ($item->status == 'Open')
                                                    <span class="badge badge-warning">{{ $item->status }}</span>
                                            @else
                                            <span class="badge badge-primary">{{ $item->status }}</span>
                                            @endif
                            </td>
                            
                        </tr>
                        @endforeach
                    </tbody>
                </table>
        </div>
    </div>
</div>
</div>
<div class="element-wrapper rejected">
        <h6 class="element-header">Rejected Job Requests</h6>

<div class="row">
    <div class="element-box-tp col-12">
           
        <div class="table-responsive">
            <table class="table table-padded">
                <thead>
                    <tr>
                        <th>Position</th>
                        <th>Job Location</th>
                        <th>Posted Date</th>
                        <th class="text-center">Close Date</th>
                        <th class="text-right">Status</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rejectedjobs as $item)
                    <tr>
                        <td class="nowrap">
                            <span class="status-pill smaller green"></span>
                                <span>{{ $item->designations->name }}</span>
                            </td>
                            <td>
                                <span>{{ $item->job_location }}</span>
                                {{-- <span class="smaller lighter">1:52am</span> --}}
                            </td>
                            <td class="cell-with-media">
                                <img alt="" src="img/company1.png" style="height: 25px;">
                                <span>{{ $item->post_date }}</span>
                            </td>
                            <td class="text-right bolder nowrap">
                                    <span class="text-success">{{ $item->cdate }}</span>
                                </td>
                            <td class="text-center">
                               
                                @if ($item->status == 'Open')
                                                    <span class="badge badge-warning">{{ $item->status }}</span>
                                            @else
                                            <span class="badge badge-primary">{{ $item->status }}</span>
                                            @endif
                            </td>
                            
                        </tr>
                        @endforeach
                    </tbody>
                </table>
        </div>
    </div>
</div>
</div>
@endpermission
<div class="element-wrapper approved">
        <h6 class="element-header">Approved Job Requests</h6>

<div class="row">
    <div class="element-box-tp col-12">
           
        <div class="table-responsive">
            <table class="table table-padded">
                <thead>
                    <tr>
                        <th>Position</th>
                        <th>Job Location</th>
                        <th>Posted Date</th>
                        <th class="text-center">Close Date</th>
                        <th class="text-right">Status</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($hodapproved as $item)
                    <tr>
                        <td class="nowrap">
                            <span class="status-pill smaller green"></span>
                                <span>{{ $item->designations->name }}</span>
                            </td>
                            <td>
                                <span>{{ $item->job_location }}</span>
                                {{-- <span class="smaller lighter">1:52am</span> --}}
                            </td>
                            <td class="cell-with-media">
                                <img alt="" src="img/company1.png" style="height: 25px;">
                                <span>{{ $item->post_date }}</span>
                            </td>
                            <td class="text-right bolder nowrap">
                                    <span class="text-success">{{ $item->cdate }}</span>
                                </td>
                            <td class="text-center">
                               
                                @if ($item->status == 'Open')
                                                    <span class="badge badge-warning">{{ $item->status }}</span>
                                            @else
                                            <span class="badge badge-primary">{{ $item->status }}</span>
                                            @endif
                            </td>
                            
                        </tr>
                        @endforeach
                    </tbody>
                </table>
        </div>
    </div>
</div>
</div>
<div class="element-wrapper rejected">
        <h6 class="element-header">Rejected Job Requests</h6>

<div class="row">
    <div class="element-box-tp col-12">
           
        <div class="table-responsive">
            <table class="table table-padded">
                <thead>
                    <tr>
                        <th>Position</th>
                        <th>Job Location</th>
                        <th>Posted Date</th>
                        <th class="text-center">Close Date</th>
                        <th class="text-right">Status</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rejectedjobs as $item)
                    <tr>
                        <td class="nowrap">
                            <span class="status-pill smaller green"></span>
                                <span>{{ $item->designations->name }}</span>
                            </td>
                            <td>
                                <span>{{ $item->job_location }}</span>
                                {{-- <span class="smaller lighter">1:52am</span> --}}
                            </td>
                            <td class="cell-with-media">
                                <img alt="" src="img/company1.png" style="height: 25px;">
                                <span>{{ $item->post_date }}</span>
                            </td>
                            <td class="text-right bolder nowrap">
                                    <span class="text-success">{{ $item->cdate }}</span>
                                </td>
                            <td class="text-center">
                               
                                @if ($item->status == 'Open')
                                                    <span class="badge badge-warning">{{ $item->status }}</span>
                                            @else
                                            <span class="badge badge-primary">{{ $item->status }}</span>
                                            @endif
                            </td>
                            
                        </tr>
                        @endforeach
                    </tbody>
                </table>
        </div>
    </div>
</div>
</div>
@stop
