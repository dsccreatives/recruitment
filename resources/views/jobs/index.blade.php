@extends('layouts.app')

@section('page-title', 'Job Vacancies')
@section('page-heading', 'Job Vacancies')

@section('breadcrumbs')
    <li class="breadcrumb-item active">
            Job Vacancies
    </li>
@stop

@section('content')

    @include('partials.messages')

    @permission('manage.jobs.manage')
    @include('jobs.rolebased.jobs')
    @endpermission
    @permission('manage.hodjobs')
    @include('jobs.rolebased.hodjobs')
    @endpermission  
    @permission('manage.hrjob')
    @include('jobs.rolebased.hrjobs')
    @endpermission  
    @permission('manage.gcfo')
    @include('jobs.rolebased.gcfojobs')
    @endpermission  
    @permission('manage.md-ceo')
    @include('jobs.rolebased.mdceojobs')
    @endpermission    
@stop
