@extends('layouts.app')

@section('page-title', 'Applicants')
@section('page-heading', 'Applicants')

@section('breadcrumbs')
    <li class="breadcrumb-item">
            Applicants
    </li>
    <li class="breadcrumb-item active">{{ $applicant->users->first_name. ' '.$applicant->users->last_name }}</li>
@stop

@section('content')
    
    <div class="card card-body">
            <div class="row">
            <div class="col-6">
                    <p class="lead">Application for {{ $applicant->jobs->designations->name }} position</p>
            </div>
            <div class="col-6">
                
                <div class="pull-right">
                <button class="btn btn-primary" type="button" data-toggle="modal" data-target=".acceptapplicant">Shortlist this Applicant</button>
                <button class="btn btn-warning" type="button" data-toggle="modal" data-target=".rejectapplicant">Reject Application</button>
            </div>
    </div>
    </div>
</div>
<div class="row">
    
    <div class="col-3">
            <div class="card card-body">
            <div class="avatar avatar-md mr-3 mt-1">
                    <span class="avatar-letter  avatar-md circle">
            <?php
             $words = explode(" ", $applicant->users->first_name. ' '.$applicant->users->last_name);
              $initials = null;
               foreach ($words as $w) {
               $initials .= $w[0];
                  }
               echo $initials; //JB
              ?>
                    </span></div>
                    <p class="text-muted lead-field-heading no-mtop">Name</p>
                    <p class="bold font-medium-xs lead-name"> {{ $applicant->users->first_name. ' '.$applicant->users->last_name }}</p>
                    <p class="text-muted lead-field-heading no-mtop">Email Address</p>
                    <p class="bold font-medium-xs lead-name"> {{ $applicant->users->email }}</p>
                    <p class="text-muted lead-field-heading no-mtop">Phone Number</p>
                    <p class="bold font-medium-xs lead-name"> {{ $applicant->users->phone }}</p>
                    <p class="text-muted lead-field-heading no-mtop">Date of Birth</p>
                    <p class="bold font-medium-xs lead-name"> {{ \Carbon\Carbon::parse($applicant->users->birthday)->format('d/M/Y') }}</p>
       
            </div> </div>
    <div class="col-9">
        <h4>Cover Letter & Resume</h4>
        <div class="card">
            <div class="card-body">
                    @if (count($files))
                    <div class="row">
                        @foreach ($files as $item)
                    <div class="col-4">
                     
                      <div class="attachment-box ripple-effect">
                        
                          <span>{{ $item->documents->name }}</span>
                          <a href="upload/user/{{ $item->filename }}">
                          <div><i class="fas fa-file-pdf fa-stack-1x"></i>  Download </div>
                        </a>  
                          <button class="btn remove-attachment" data-tippy-placement="top" data-tippy="" data-original-title="Remove">
                            <i class="fas fa-trash"></i>
                          </button>
                         
                       
                      </div>
                     
                      
                    </div>
                    @endforeach
                    </div>
                    @else
                <div class="row">
                    <div class="col-8">
                        @if ($applicant->coverletter)
                        {!! $applicant->coverletter !!}
                        @else
                            Sorry the Applicant didnt Write or attach a Cover Letter
                        @endif
                            
                    </div>
                   <div class="col-4">
                        <div class="attachment-box ripple-effect">
                        
                                <span>Resume</span>
                                <a href="upload/user/{{ $applicant->resume }}">
                                <div><i class="fas fa-file-pdf fa-stack-1x"></i>  Download </div>
                              </a>  
                                <button class="btn remove-attachment" data-tippy-placement="top" data-tippy="" data-original-title="Remove">
                                  <i class="fas fa-trash"></i>
                                </button>
                               
                             
                            </div>
                   </div>
                </div>
                @endif
            </div>
        </div>
        <h4>Work Experience</h4>
        <div class="card">
            <div class="card-body">
                <div class="row">
                        <div class="col-12" id="work-experience">
                                @foreach($works as $item)
                                  
                               
                                 <h5>Employer: {{ $item->employer}}</h5>
                                 
                                  <ul class="work-ex">
                                     <li><span class="heading">Designation</span><span class="body-text"> {{$item->designations->name}}  </span></li>
                                     <li><span class="heading">Start Date <i class="fas fa-calendar-alt"></i></span><span class="body-text">{{$item->startdate}}</span></li>
                                      <li><span class="heading">End Date <i class="fas fa-calendar-alt"></i></span><span class="body-text">{{$item->enddate}}</span></li>
                                       <li><span class="heading">Country of Employment <i class="fas fa-globe"></i></span><span class="body-text">{{$item->countries->name}}</span></li>
                                 </ul>
                            
                               
                                 
                                 @endforeach
                                  </div>
                   
                </div>
            </div>
        </div>

        <h4>Education Background</h4>
        <div class="card">
            <div class="card-body">
                <div class="row">
        <div class="col-12" id="work-experience">
                @foreach($education as $key)
   
                <h5>Name of the School: {{ $key->institution}}</h5>
                 
                <ul class="work-ex">
                   <li><span class="heading">Area of Study</span><span class="body-text"> {{ $key->studies->name}}  </span></li>
                    <li><span class="heading">Achieved <i class="fas fa-user-graduate "></i> </span><span class="body-text"> {{$key->achievement->name}}  </span></li>
                   <li><span class="heading">Start Date <i class="fas fa-calendar-alt"></i></span><span class="body-text">{{$key->startdate}}</span></li>
                    <li><span class="heading">End Date <i class="fas fa-calendar-alt"></i></span><span class="body-text">{{$key->enddate}}</span></li>
                     <li><span class="heading">Country of Study <i class="fas fa-globe"></i></span><span class="body-text">{{$key->countries->name}}</span></li>
               </ul>
            
               
                 
                 @endforeach
        </div>
    </div>
</div>
</div>

    </div>
</div>
<div class="modal fade acceptapplicant" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
          <div class="modal-content">
                <div class="modal-body">
            <form action="{{ route('shortlisting.storing', $applicant->id) }}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-12">
                            <div class="form-group">
                                    <label for="">Reason for Accepting</label>
                                    <textarea class="form-control" name="reason"></textarea>
                
                                </div>
                                <button type="submit" class="btn btn-primary">Accept</button>
                    </div>
                </div>
            </form>
                </div>
          </div>
        </div>
      </div>
      <div class="modal fade rejectapplicant" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl">
              <div class="modal-content">
                    <div class="modal-body">
                <form action="" method="post">
                    <div class="row">
                        <div class="col-12">
                                <div class="form-group">
                                        <label for="">Reason for Rejecting</label>
                                        <textarea class="form-control" name="reason"></textarea>
                    
                                    </div>
                                    <button type="submit" class="btn btn-primary">Reject</button>
                        </div>
                    </div>
                </form>
                    </div>
              </div>
            </div>
          </div>
@stop