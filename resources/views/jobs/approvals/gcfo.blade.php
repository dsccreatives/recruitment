@extends('layouts.app')

@section('page-title', 'Job Request Approve '. $hr->designations->name . 'Position')
@section('page-heading', 'System Notifications')

@section('breadcrumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('user.list') }}">@lang('app.users')</a>
    </li>
    <li class="breadcrumb-item active">
            Job Request Approve
    </li>
@stop

@section('content')

<div class="row">
    <div class="col-md-8">
            <h4>{{ $hr->designations->name }}</h4>
        <div class="card">
            <div class="card-body">
                    <div class="rendered-text-body">
                            {!! $hr->designations->jd !!}
                    </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        
        @if ($hr->stage_id ==7)
        <div class="alert alert-primary" role="alert">
              You have already approved this Job Request
              </div>
        @elseif($hr->stage_id ==9)
        @if($hr->user_id ==auth()->user()->id)
        
        <div class="alert alert-danger" role="alert">
                You have Rejected this Job Request
        </div>
        @else
        <div class="alert alert-danger" role="alert">
               This Job Request was Rejected by {{ $hr->user->first_name }} in {{ $hr->user->role->name }} Department
        </div>
        @endif
        @else
       
       <div class="row">
           <div class="col">
               <button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#approve">Approve</button>
                   </div>
                   <div class="col">
               <button class="btn btn-block btn-warning" data-toggle="modal" data-target="#reject">Reject</button>

               </div>
            
            
       </div>
       @endif
       <hr>
        <h4> <i class="far fa-file-alt"></i> Job Summary</a></h4>
        <hr>
        <div class="row">
            <div class="col-sm-5"><b>Experience</b>
                </div>
                <div class="col-sm-7"><p>{{ $hr->experience }}</div>
        </div>
        <div class="row">
                <div class="col-sm-5"><b>Job Type</b>
                    </div>
                    <div class="col-sm-7"><p>{{ $hr->job_type }}</div>
    </div>
        <div class="row">
                    <div class="col-sm-5"><b>Location</b>
                        </div>
                        <div class="col-sm-7"><p>{{ $hr->job_location }}</div>
            </div>
<div class="row">
                        <div class="col-sm-5"><b>Salary Range</b>
                            </div>
                            <div class="col-sm-7"><p>{{ $hr->salary_range }}</div>
 </div>

<div class="row">
        <div class="col-sm-5"><b>Post Date</b>
            </div>
            <div class="col-sm-7"><p>{{ $hr->post_date }}</div>
</div>
<div class="row">
        <div class="col-sm-5"><b>Deadline to Apply</b>
            </div>
            <div class="col-sm-7"><p>{{ $hr->ldate }}</div>
</div>
<div class="row">
        <div class="col-sm-5"><b>Closing Date</b>
            </div>
            <div class="col-sm-7"><p>{{ $hr->cdate }}</div>
</div>
<div class="row">
        <div class="col-sm-5"><b>Skills Required</b>
            </div>
            <div class="col-sm-7"><p> @foreach ($hr->skills as $item)
                    <span class="badge badge-success"> 
                              {{ $item->name }}
                        </span>
                    @endforeach     </div>
</div>

<div class="row">
        <div class="col-sm-12"><b>Short Description</b>
            </div>
            <div class="col-sm-12"><p style="text-align:justify">{{ $hr->short_description }}</div>
</div>
    </div>
</div>
<!-- Approval Modal -->
<div class="modal fade" id="approve" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Job Approve Confirmation</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
             <form action="{{ route('approviing.job.gcfo',$hr->id) }}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <div class="form-group">
                     <label for="">Reason/Comments for Approval</label>
                     <textarea class="form-control" name="reason" id="" cols="30" rows="10"></textarea>
                 </div>
                 <button type="submit" class="btn btn-primary">Approve</button>
             </form>
            </div>
           
          </div>
        </div>
      </div>

<!-- Reject Modal -->
<div class="modal fade" id="reject" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Job Reject Confirmation</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
             <form action="{{ route('rejected.job.gcfo',$hr->id) }}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <div class="form-group">
                     <label for="">Reason/Comments for Disapproval/Rejection</label>
                     <textarea class="form-control" name="reason" id="" cols="30" rows="10"></textarea>
                 </div>
                 <button type="submit" class="btn btn-primary">Disapproved</button>
             </form>
            </div>
           
          </div>
        </div>
      </div>
@stop    