@extends('layouts.app')

@section('page-title', 'Applicants')
@section('page-heading', 'Applicants')

@section('breadcrumbs')
    <li class="breadcrumb-item active">
            Applicants
    </li>
@stop

@section('content')

    @include('partials.messages')

    <div class="card">
        <div class="card-body">
                <div class="row">
                        <div class="table-responsive">
                            <table class="table table-padded" id="example">
                                <thead>
                                    <th></th>
                                    <th>Full Names</th>
                                    <th>Job Applied</th>
                                    <th>Job Status</th>
                                    <th>Resume</th>
                                    <th>Approval Status</th>
                                    <th>Actions</th>
                                </thead>
                                <tbody>
                                        @foreach ($applicants as $item)
                                    <tr>
                                        <td><img src="{{ $item->users->avatar }}" alt="">
                                            </div></td>
                                            <td>{{ $item->users->first_name. ' '.$item->users->last_name  }}</td>
                                            <td>{{ $item->jobs->designations->name }}</td>
                                            <td>
                                            @if ($item->jobs->status=='Open')
                                                <span class="label label-success  s-status">Open</span>
                                                @else
                                                <span class="label label-danger  s-status">Closed</span>
                                            @endif
                                            </td>
                                            <td><a href="{{ url('uploads/resume', $item->resume) }}">Download Resume</a></td>
                                            <td>@if ($item->approvedby_id==null)
                                                <span class="label label-primary">Under Review</span>
                                            @endif</td>
                                            <td><a href="{{ route('show.applicant', $item->id ) }}"><span class="label label-primary"><i class="fa fa-eye"></i> View Applicant</span></a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                       
                    </div>
        </div>
    </div>
    @stop