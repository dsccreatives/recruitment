@extends('layouts.app')

@section('page-title', 'System Notifications')
@section('page-heading', 'System Notifications')

@section('breadcrumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('user.list') }}">@lang('app.users')</a>
    </li>
    <li class="breadcrumb-item active">
            System Notifications
    </li>
@stop

@section('content')

<div class="row">
    <div class="col-md-6 offset-md-3">
        <div class="card">
            <div class="card-body">
                <h4>Notifications</h4>
                <a href="#" onclick="mark_all_notifications_as_read_inline(); return false;">Mark all as read</a>
                <div id="notifications">
                    @foreach ($notifications as $item)
                       <div class="notification-wrapper" data-notification-id="24">
                            <div class="notification-box-all">
                                <div class="media-body notification_link" data-link="https://www.perfexcrm.com/demo/admin/tickets/ticket/7">
                                    <div class="description">{{ $item->data['message'] }}</div>
                                    <small class="text-muted text-right text-has-action" data-placement="right" data-toggle="tooltip" data-title="2019-01-19 21:48:17" data-original-title="" title="">4 hrs ago</small>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        </div>
            </div>
        </div>
    </div>
</div>
@stop    