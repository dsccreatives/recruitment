

@if (count($files))
<div class="row">
    @foreach ($files as $item)
<div class="col-4">
 
  <div class="attachment-box ripple-effect">
    
      <span>{{ $item->documents->name }}</span>
      <a href="upload/user/{{ $item->filename }}">
      <div><i class="fas fa-file-pdf fa-stack-1x"></i>  Download </div>
    </a>  
      <button class="btn remove-attachment" data-tippy-placement="top" data-tippy="" data-original-title="Remove">
        <i class="fas fa-trash"></i>
      </button>
     
   
  </div>
 
  
</div>
@endforeach
</div>
@else
<div class="row">
  
    <div class="col-12">
        <div class="alert alert-info" role="alert">
            Not you can Upload new Resume, Cover Letter and Academic Certificates
          </div>
          
    </div>

      <div class="col-12">
          
              {!! Form::open(['route' => 'filesrequired.store', 'method' => 'POST', 'id' => 'docdetails-form', 'enctype'=>'multipart/form-data']) !!}
                            
            <div class="row">
                @foreach ($docs as $item)
                <div class="col">
                   <div class="form-group">
                       <label style="display: block;">{{ $item->name }}</label>
                   <input class="form-control" type="file" name="document_id[{{ $item->id }}][]">
                     </div>
                </div>
               
         @endforeach
        </div>
         <button type="submit" class="btn btn-primary is-info is-outlined">Upload Documents</button>
       
        </form>
     
{{--     
     <div class="col">
        <div class="form-group">
          <label style="display: block;">Cover Letter</label>
          <input class="form-control" type="file" name="coverletter">
        </div>
     </div>
     <div class="col">
        <div class="form-group">
          <label style="display: block;">Zip all Professional Certificates and attach the zip here</label>
          <input class="form-control" type="file" name="certs">
        </div>
        </div> --}}
      
    </div>
      
     
</div>
@endif

  {{-- <div class="col-4">
      <div class="attachment-box ripple-effect">
      
          <span>Resume</span>
          <a href="{{ $user->resume }}">
          <div><i class="fas fa-file-pdf fa-stack-1x"></i>  Download </div>
        </a>  
          <button class="btn remove-attachment" data-tippy-placement="top" data-tippy="" data-original-title="Remove">
            <i class="fas fa-trash"></i>
          </button>
         
       
      </div>
      
  
  </div>
  <div class="col-4">
      <div class="attachment-box ripple-effect">
      
          <span>Cover Letter</span>
          <a href="{{ $user->coverletter }}">
          <div><i class="fas fa-file-word fa-stack-1x"></i>  Download </div>
        </a>  
          <button class="btn remove-attachment" data-tippy-placement="top" data-tippy="" data-original-title="Remove">
            <i class="fas fa-trash"></i>
          </button>
         
       
      </div>

   
  </div>
  <div class="col-4">
    
      <div class="attachment-box ripple-effect">
      
          <span>Certificates</span>
          <a href="{{$user->certs}}">
          <div><i class="fas fa-file-archive fa-stack-1x"></i>  Download </div>
        </a>  
          <button class="btn remove-attachment" data-tippy-placement="top" data-tippy="" data-original-title="Remove">
              <i class="fas fa-trash"></i>
          </button>
         
       
      </div>
    
  </div> --}}


