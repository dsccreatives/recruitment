<nav class="navbar fixed-top align-items-start navbar-expand-lg pl-0 pr-0 py-0" >

    <div class="navbar-brand-wrapper d-flex align-items-center justify-content-center">
        <a class="navbar-brand mr-0" href="{{ url('/') }}">
            <img src="{{ url('assets/img/vanguard-logo.png') }}" height="35" alt="{{ settings('app_name') }}">
        </a>
    </div>

    <div>
        <button class="navbar-toggler" type="button" id="sidebar-toggle">
            <i class="fas fa-align-right text-muted"></i>
        </button>

        <button class="navbar-toggler mr-3"
                type="button"
                data-toggle="collapse"
                data-target="#top-navigation"
                aria-controls="top-navigation"
                aria-expanded="false"
                aria-label="Toggle navigation">
            <i class="fas fa-bars text-muted"></i>
        </button>
    </div>

    <div class="collapse navbar-collapse" id="top-navigation">
        <div class="row ml-2">
            <div class="col-lg-12 d-flex align-items-center py-3">
                <h4 class="page-header mb-0">
                    @yield('page-heading')
                </h4>

                <ol class="breadcrumb mb-0 font-weight-light">
                    <li class="breadcrumb-item">
                        <a href="{{ url('/') }}" class="text-muted">
                            <i class="fa fa-home"></i>
                        </a>
                    </li>

                    @yield('breadcrumbs')
                </ol>
            </div>
        </div>
        <ul class="navbar-nav">
                @permission('designation.manage')
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('designation*') ? 'active' : '' }}"
                       href="{{ route('designations.index') }}">Designations</a>
                </li>
                @endpermission
                
                @permission('jobs.manage')
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('jobs*') ? 'active' : '' }}"
                       href="{{ route('jobs.index') }}">Job Vacancies</a>
                </li>
                @endpermission
                @permission('jobs.manage')
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('applicants*') ? 'active' : '' }}" href="{{ route('view.applicants') }}"> View Applicants</a>
                </li>
                @endpermission
        </ul>

        <ul class="navbar-nav ml-auto pr-3">
                <li class="nav-item dropdown notifications-wrapper header-notifications" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Notifications">
                    <a href="#" style="margin-top: 12px;" id="navbarDropdown" class="nav-link dropdown-toggle notifications-icon" data-toggle="dropdown" aria-expanded="false">
                        <i class="far fa-bell fa-fw fa-lg"></i>
                           @if (auth()->user()->unreadnotifications->count())
                           <span class="label icon-total-indicator bg-warning icon-notifications">
                                {{ auth()->user()->unreadnotifications->count() }}</span>
                           
                           @endif
                        </a>
                      <ul class="dropdown-menu notifications animated fadeIn width400" data-total-unread="1">
                        <li class="not_mark_all_as_read">
                          <a href="{{ route('readnotifications') }}">Mark all as read</a>
                        </li>
                            @if (auth()->user()->notifications)
                            @foreach (auth()->user()->notifications as $item)
                            <li class="relative notification-wrapper" data-notification-id="24">
                                    <a href="#" class="notification-top notification-link">
                                      <div class="notification-box ">
                                        <div class="media-body">
                                <span class="notification-title">{{ $item->data['message'] }}</span><br>
                              <small class="text-muted">
                                <span class="text-has-action" data-placement="right" data-toggle="tooltip" data-title="{{ $item->created_at->toDayDateTimeString() }}">
                                        {{ $item->created_at->toDayDateTimeString() }}         </span>
                              </small>
                            </div>
                          </div>
                              </a>
                              <a href="#" class="text-muted pull-right not-mark-as-read-inline" onclick="set_notification_read_inline(24);" data-placement="left" data-toggle="tooltip" data-title="Mark as Read"><small><i class="fa fa-circle-thin" aria-hidden="true"></i></small></a>
                        </li>
                            @endforeach
                            @else
                            @foreach (auth()->user()->unreadnotifications as $item)
                            <li class="relative notification-wrapper" data-notification-id="24">
                                    <a href="#" class="notification-top notification-link">
                                      <div class="notification-box unread">
                                        <div class="media-body">
                                <span class="notification-title">{{ $item->data['message'] }}</span><br>
                              <small class="text-muted">
                                <span class="text-has-action" data-placement="right" data-toggle="tooltip" data-title="{{ $item->created_at->toDayDateTimeString() }}">
                                        {{ $item->created_at->toDayDateTimeString() }}         </span>
                              </small>
                            </div>
                          </div>
                              </a>
                              <a href="#" class="text-muted pull-right not-mark-as-read-inline" onclick="set_notification_read_inline(24);" data-placement="left" data-toggle="tooltip" data-title="Mark as Read"><small><i class="fa fa-circle-thin" aria-hidden="true"></i></small></a>
                        </li>
                            @endforeach
                            @endif
                         
                         
                        <li class="divider no-mbot"></li>
                      <li class="text-center">
                            <a href="{{ route('notifications.index') }}">View all notifications</a>
                        </li>
                      </ul>
                      </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle"
                   href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false">
                   Welcome,  {{ auth()->user()->present()->nameOrEmail }} 
                   <img src="{{ auth()->user()->present()->avatar }}"
                        width="50"
                        height="50"
                        class="rounded-circle img-thumbnail img-responsive">
                </a>
                <div class="dropdown-menu dropdown-menu-right position-absolute" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('profile') }}">
                        <i class="fas fa-user text-muted mr-2"></i>
                        @lang('app.my_profile')
                    </a>
                    <a class="dropdown-item" href="{{ url('/dashboard') }}">
                        <i class="fas fa-desktop"></i>
                        View Dashboard
                        </a>
                       
                    @if(auth()->user()->role_id ==1)
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ url('/dashboard')}}">
                      Admin Dashboard
                    </a>
                    @endif
                    <div class="dropdown-divider"></div>

                    <a class="dropdown-item" href="{{ route('auth.logout') }}">
                        <i class="fas fa-sign-out-alt text-muted mr-2"></i>
                        @lang('app.logout')
                    </a>
                </div>
            </li>
        </ul>
    </div>
</nav>