<!-- Modal -->
<div class="modal fade" id="candidateapply" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">You are applying for {{$job->designations->name}} Position</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{ route('apply.job', $job->id) }}" method="POST" >
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-12">
                  <div class="form-group">
                    <label for="">Write your Cover Letter here</label>
                    <textarea name="coverletter" id="summernote" style="width:100%:" class="form-control" cols="30" rows="20"></textarea>
                  
                  </div>

                  @if (Auth::check() && Auth::user()->$files==null)
                      <label for="">Please Upload a Resume:</label>
                           
            <div class="row">
                @foreach ($docs as $item)
                @if ($item->id ==1)
                <div class="col">
                    <div class="form-group">
                        <label style="display: block;">{{ $item->name }}</label>
                    <input class="form-control" required type="file" name="resume">
                      </div>
                 </div>
                @endif
                
               
         @endforeach
        </div>
         
                  @endif
                    </div>
                </div>
          
           
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" id="applying">Send Application</button>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>