<?php

namespace Boaz;

use Illuminate\Database\Eloquent\Model;
use Boaz\User;
use Boaz\Role;

class Job extends Model
{
    //
    protected $id = ['id'];
    protected $dates = [
        'jreference'
    ];
    const HOD = 1;
    const MD = 2;
    const HR = 6;
    const GCFO = 7;
    const NOTAPPROVED = 8;
    const REJECTED =9;
    const FINALAPPROVAL =10;

    public static function headofdepartment()
    {
        return self::where('stage_id',self::HOD);
    }
    public static function finalapproval()
    {
        return self::where('finalapproval',self::FINALAPPROVAL);
    }
    public static function notapprovedyet()
    {
        return self::where('stage_id',self::NOTAPPROVED);
    }
    public static function rejected()
    {
        return self::where('stage_id',self::REJECTED);
    }
    public static function managingdirector()
    {
        return self::where('stage_id',self::MD);
    }
    public static function humanresource()
    {
        return self::where('stage_id',self::HR);
    }
    public static function groupchieffinanceofficer()
    {
        return self::where('stage_id',self::GCFO);
    }

    public function skills(){
        return $this->belongsToMany('Boaz\Skill', 'job_skill', 'job_id');
    }
    public function designations()
    {
        return $this->hasOne('Boaz\Designations', 'id', 'designation_id');
    }

    public function stages()
    {
        return $this->hasOne('Boaz\Stage', 'id', 'stage_id');
    }
    public function user()
    {
      return $this->belongsTo(User::class);
    }

    public function role()
    {
      return $this->belongsTo(Role::class);
    }
}
