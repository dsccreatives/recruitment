<?php

namespace Boaz\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Boaz\Job;
use Auth;

class ApproveJobs extends Notification
{
    use Queueable;
    public $jobs;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($jobs)
    {
        //
        $this->jobs = $jobs;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $subject = sprintf("[%s] %s", settings('app_name'), 'Job Request to Approve');
        return (new MailMessage)
                     ->subject($subject)
                    ->line('Hello, We have received a new Job Request to Approve')
                    ->action('click this link to Approve/Disapprove', url('/'),'the Job Request');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [ 'message' => 'A new Job Request was published.',
             'action' => route('approve.job.hod', $this->jobs->id)
        ];
    }
}