<?php

namespace Boaz\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Boaz\Job;
use Auth;

class RejectJobRequest extends Notification
{
    
    use Queueable;
    public $job;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($job)
    {
        //
        $this->jobs = $job;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $subject = sprintf("[%s] %s", settings('app_name'), 'Job Request Rejected');
        return (new MailMessage)
                     ->subject($subject)
                    ->line('Hello, you have disapproved Job Request');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [ 'message' => 'Job Request Rejected.',
             'action' => route('approve.job.hod', $this->jobs->id)
        ];
    }
}