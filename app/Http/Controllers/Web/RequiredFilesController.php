<?php

namespace Boaz\Http\Controllers\Web;

use Illuminate\Http\Request;
use Boaz\Http\Controllers\Controller;
use Boaz\File;
use Auth;
use Carbon\Carbon;

class RequiredFilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = File::all();
        return view('my-documents.list', compact($data));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
     
        foreach($request->document_id as  $key =>  $document) { 

            $fileobject = $document[0];

            //getting extesion
            $file_type = $fileobject->getClientOriginalExtension();
            //renaming the file
            $file_name = Auth::user()->email.'-'.Carbon::now()->format('Ymdhis').'-'.$fileobject->getClientOriginalExtension();

            //destination
            $destinationPath = public_path('/upload/user');
            $fileobject->move($destinationPath, $file_name);

            File::create([
                'user_id' => auth::id(),
                'document_id' => $key,
                'filename' =>$file_name,
                ]
            );
            toastr()->success('Your Documents Have been uploaded successfully');
            return redirect()->back();

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
