<?php

namespace Boaz\Http\Controllers\Web;

use Boaz\Http\Controllers\Controller;
use Boaz\Repositories\Activity\ActivityRepository;
use Boaz\Repositories\User\UserRepository;
use Boaz\Support\Enum\UserStatus;
use Auth;
use Carbon\Carbon;
use Config;
use Boaz\Job;
use Boaz\JobsApplications;

class DashboardController extends Controller
{
    /**
     * @var UserRepository
     */
    private $users;
    /**
     * @var ActivityRepository
     */
    private $activities;

    /**
     * DashboardController constructor.
     * @param UserRepository $users
     * @param ActivityRepository $activities
     */
    public function __construct(UserRepository $users, ActivityRepository $activities)
    {
        $this->middleware('auth');
        $this->users = $users;
        $this->activities = $activities;
    }

    /**
     * Displays dashboard based on user's role.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (Auth::user()->hasRole('Admin')) {
            return $this->adminDashboard();
        }
        
        if(Auth::user()->hasRole('Applicant')) {
            return $this->defaultDashboard();
        }
        return $this->bossesDashboard();
    }

    public function bossesDashboard()
    {    $vacanciesnotapproved = Job::notapprovedyet()->get();
        $hodapproved = Job::headofdepartment()->where('hod', 1)->get();
        $hrapproved = Job::where('hr', 6)->get();
        $mdapproved = Job::where('md', 2)->get();
        $gcfoapproved = Job::where('gcfo', 7)->get();
        $rejectedjobs = Job::rejected()->get();
        $approvedjobs = Job::finalapproval()->get();
        
        $greetings = "";

    /* This sets the $time variable to the current hour in the 24 hour clock format */
    $time = date("H");

    /* Set the $timezone variable to become the current timezone */
    $timezone = Config::get('app', 'timezone');

    /* If the time is less than 1200 hours, show good morning */
    if ($time < "12") {
        $greetings = "Good morning";
    } else

    /* If the time is grater than or equal to 1200 hours, but less than 1700 hours, so good afternoon */
    if ($time >= "12" && $time < "17") {
        $greetings = "Good afternoon";
    } else

    /* Should the time be between or equal to 1700 and 1900 hours, show good evening */
    if ($time >= "17" && $time < "20") {
        $greetings = "Good evening";
    } else

    /* Finally, show good night if the time is greater than or equal to 1900 hours */
    if ($time >= "20") {
        $greetings = "Good night";
    }
        return view('dashboard.bosses',compact('vacanciesnotapproved','approvedjobs','mdapproved','gcfoapproved','hrapproved', 'hodapproved', 'rejectedjobs', 'greetings'));
    }

    /**
     * Displays dashboard for admin users.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function adminDashboard()
    {
        $usersPerMonth = $this->users->countOfNewUsersPerMonth(
            Carbon::now()->subYear()->startOfMonth(),
            Carbon::now()->endOfMonth()
        );

        $stats = [
            'total' => $this->users->count(),
            'new' => $this->users->newUsersCount(),
            'banned' => $this->users->countByStatus(UserStatus::BANNED),
            'unconfirmed' => $this->users->countByStatus(UserStatus::UNCONFIRMED)
        ];

        $latestRegistrations = $this->users->latest(6);

        return view('dashboard.admin', compact('stats', 'latestRegistrations', 'usersPerMonth'));
    }

    /**
     * Displays default dashboard for non-admin users.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function defaultDashboard()
    {
        $activities = $this->activities->userActivityForPeriod(
            Auth::user()->id,
            Carbon::now()->subWeeks(2),
            Carbon::now()
        )->toArray();
        $pastjobs = JobsApplications::where('user_id', '=', Auth::user()->id)->count();
        $pastjob = JobsApplications::where('user_id', '=', Auth::user()->id)->get();
        return view('dashboard.default', compact('activities', 'pastjobs', 'pastjob'));
    }
}
