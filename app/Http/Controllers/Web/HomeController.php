<?php

namespace Boaz\Http\Controllers\Web;

use Artisan;
use DB;
use Dotenv\Dotenv;
use Illuminate\Http\Request;
use Boaz\File as Files;
use Auth;
use Boaz\Document;
use Input;
use Session;
use Settings;
use Boaz\Job;
use Boaz\Http\Controllers\Controller;
use Boaz\Http\Requests;

class HomeController extends Controller
{
    public function index()
    {
    	$jobs =  Job::finalapproval()->get();
        return view('home.index', compact('jobs'));
    }

    public function jobDetail($id)

    {
        $job=Job::find($id);
        $docs = Document::all();
        //$user = Auth::user();
        $files = Files::where('user_id',  auth::id())->first();
        // if($user->isdesign($files==null))
		// {
        //     return redirect()->route('jobDetail', $job->id)
        //     ->withSuccess('You have not uploaded any Resume in your Profile');
        //     exit;
           
        // }
    	if($job){
            return view('home.job-details',compact('job','files','docs'));
        }else{
            return redirect('apply-job')->with([
                'message'=> 'Job Details Not found'
            ]);
        }
    }
   
}
