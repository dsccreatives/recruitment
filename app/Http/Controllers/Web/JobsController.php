<?php

namespace Boaz\Http\Controllers\Web;

use Illuminate\Http\Request;
use Boaz\Http\Controllers\Controller;
use Boaz\Job;
use Boaz\WorkExperience;
use Boaz\File as Files;
use Boaz\Designations;
use Boaz\Skill;
use Boaz\Stage;
use Boaz\JobApprovals;
use Auth;
use Boaz\JobsApplications;
use Carbon\Carbon;
use Boaz\ProfessionalCertification;
use Boaz\EducationBackground;
use Boaz\Notifications\JobApplied;
use Boaz\Notifications\ApproveJobs;
use Boaz\Notifications\RejectJobRequest;
use Boaz\Repositories\User\UserRepository;
use Boaz\User;

class JobsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $vacanciesnotapprovedbyhod = Job::headofdepartment()->where('hod', '=',null)->get();
        $vacanciesnotapprovedbyhr = Job::headofdepartment()->where('hod', 1)->get();
        $vacanciesnotapprovedbygcfo = Job::humanresource()->where('hr', 6)->get();
        $vacanciesnotapprovedbymd = Job::groupchieffinanceofficer()->where('gcfo', 7)->get();

        $hodapproved = Job::headofdepartment()->get();
        $hrapproved = Job::humanresource()->get();
        $gcfoapproved = Job::groupchieffinanceofficer()->get();
        $mdapproved = Job::managingdirector()->get();
        $rejectedjobs = Job::rejected()->get();
        $design = Designations::all();
        $skills = Skill::all();
        return view('jobs.index', compact('vacanciesnotapprovedbyhod','gcfoapproved', 'vacanciesnotapprovedbyhr','vacanciesnotapprovedbygcfo','vacanciesnotapprovedbymd','mdapproved', 'hrapproved','hodapproved','rejectedjobs', 'design','skills'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $jobs = new Job();
        $jobs->designation_id = $request->get('designation_id');
        $jobs->no_position = $request->get('no_position');
        $jobs->job_type = $request->get('job_type');
        $jobs->experience = $request->get('experience');
        $jobs->age = $request->get('age');
        $jobs->job_location = $request->get('job_location');
        $jobs->salary_range = $request->get('salary_range');
        $jobs->post_date = $request->get('post_date');
        $jobs->ldate = $request->get('ldate');
        $jobs->cdate = $request->get('cdate');
        $jobs->status = $request->get('status');
        $jobs->jreference = Carbon::today();
        $jobs->stage_id = '8';
        $jobs->short_description = $request->get('short_description');
        $jobs->user_id = Auth::user()->id;
        
        $jobs->save();
        $jobs->skills()->sync($request->skill_id, false);
        \Notification::send(User::where('role_id',3)->get(), new ApproveJobs($jobs));
    //    $users =User::where('role_id',3)->get();
    //     $users->notify(new ApproveJobs($jobs));
        toastr()->success('Job Vacancy added successfuly');
        return redirect()->route('jobs.index');
    }
//Approvals from Hod
    public function approvejobhod($id) {
        $hod = Job::findOrfail($id);
        
        return view('jobs.approvals.hod',compact('hod'));
    }
    public function approved(Request $request, $id) {
        $job = Job::where('id', $id)->first();
        $approve = new JobApprovals();
        $approve->user_id = auth::id();
        $approve->reason = $request->get('reason');
        $approve->stage_id = Job::HOD;
        $approve->job_id = $job->id;
        $approve->status = "Approved by HOD";
        $approve->save();
        $job->stage_id = Job::HOD;
        $job->hod = Job::HOD;
        $job->update();
        toastr()->success('You have Approved this Job Request Successfully');
            return redirect()->back();
    }

    public function rejected(Request $request, $id) {
        $job = Job::where('id', $id)->first();
        $approve = new JobApprovals();
        $approve->user_id = auth::id();
        $approve->reason = $request->get('reason');
        $approve->stage_id = Job::REJECTED;
        $approve->job_id = $job->id;
        $approve->status = "Rejected by HOD";
        $approve->save();
        $job->stage_id = Job::REJECTED;
        $job->update();
        \Notification::send(User::where('role_id',3)->get(), new RejectJobRequest($job));
        toastr()->info('You have Rejected this Job Request');
            return redirect()->back();
    }

    //Approvals from HR
    public function approvejobhr($id) {
        $hr = Job::findOrfail($id);
        
        return view('jobs.approvals.hr',compact('hr'));
    }
    public function approvedhr(Request $request, $id) {
        $job = Job::where('id', $id)->first();
        $approve = new JobApprovals();
        $approve->user_id = auth::id();
        $approve->reason = $request->get('reason');
        $approve->stage_id = Job::HR;
        $approve->job_id = $job->id;
        $approve->status = "Approved by HR";
        $approve->save();
        $job->stage_id = Job::HR;
        $job->hr = Job::HR;
        $job->update();
        toastr()->success('You have Approved this Job Request Successfully');
            return redirect()->back();
    }

    public function rejectedhr(Request $request, $id) {
        $job = Job::where('id', $id)->first();
        $approve = new JobApprovals();
        $approve->user_id = auth::id();
        $approve->reason = $request->get('reason');
        $approve->stage_id = Job::REJECTED;
        $approve->job_id = $job->id;
        $approve->status = "Rejected by HOD";
        $approve->save();
        $job->stage_id = Job::REJECTED;
        $job->update();
        \Notification::send(User::where('role_id',3)->get(), new RejectJobRequest($job));
        toastr()->info('You have Rejected this Job Request');
            return redirect()->back();
    }

    //Approvals from GCFO
    public function approvejobgcfo($id) {
        $hr = Job::findOrfail($id);
        
        return view('jobs.approvals.gcfo',compact('hr'));
    }
    public function approvedgcfo(Request $request, $id) {
        $job = Job::where('id', $id)->first();
        $approve = new JobApprovals();
        $approve->user_id = auth::id();
        $approve->reason = $request->get('reason');
        $approve->stage_id = Job::GCFO;
        $approve->job_id = $job->id;
        $approve->status = "Approved by GCFO";
        $approve->save();
        $job->stage_id = Job::GCFO;
        $job->gcfo = Job::GCFO;
        $job->update();
        toastr()->success('You have Approved this Job Request Successfully');
            return redirect()->back();
    }

    public function rejectedgcfo(Request $request, $id) {
        $job = Job::where('id', $id)->first();
        $approve = new JobApprovals();
        $approve->user_id = auth::id();
        $approve->reason = $request->get('reason');
        $approve->stage_id = Job::REJECTED;
        $approve->job_id = $job->id;
        $approve->status = "Rejected by GCFO";
        $approve->save();
        $job->stage_id = Job::REJECTED;
        $job->update();
        \Notification::send(User::where('role_id',3)->get(), new RejectJobRequest($job));
        toastr()->info('You have Rejected this Job Request');
            return redirect()->back();
    }

    //Approvals from MD/CEO
    public function approvejobmd($id) {
        $md = Job::findOrfail($id);
        
        return view('jobs.approvals.md',compact('md'));
    }
    public function approvedmd(Request $request, $id) {
        $job = Job::where('id', $id)->first();
        $approve = new JobApprovals();
        $approve->user_id = auth::id();
        $approve->reason = $request->get('reason');
        $approve->stage_id = Job::MD;
        $approve->job_id = $job->id;
        $approve->status = "Final Approval by MD/CEO";
        $approve->save();
        $job->stage_id = Job::MD;
        $job->md = Job::MD;
        $job->update();
        toastr()->success('You have Approved this Job Request Successfully');
            return redirect()->back();
    }

    public function rejectedmd(Request $request, $id) {
        $job = Job::where('id', $id)->first();
        $approve = new JobApprovals();
        $approve->user_id = auth::id();
        $approve->reason = $request->get('reason');
        $approve->stage_id = Job::REJECTED;
        $approve->job_id = $job->id;
        $approve->status = "Rejected by GCFO";
        $approve->save();
        $job->stage_id = Job::REJECTED;
        $job->update();
        \Notification::send(User::where('role_id',3)->get(), new RejectJobRequest($job));
        toastr()->info('You have Rejected this Job Request');
            return redirect()->back();
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function applyjob(Request $request, $id)
    {
        
        $user = Auth::user();
        $job = Job::where('id', $id)->first();
        $files = Files::where('user_id',  $user->id)->first();
        $works =WorkExperience::where('user_id', $user->id)->first();
        $education = EducationBackground::where('user_id', $user->id)->first();
        $pcertification = ProfessionalCertification::where('user_id', $user->id)->first();

        if($user->isAppliedOnJob($job->id))
		{
            return redirect()->route('jobDetail', $job->id)
            ->withSuccess('You have already applied for this job');
            exit;
           
        }
       
        if ($works==null) {
            return redirect()->route('profile')
            ->withSuccess('You have not added any past Work Experience in your Profile');
            exit;
        }
       
        if($education==null)
		{
          return redirect()->route('profile')
            ->withSuccess('You have not added any Education Background in your Profile');
            exit;
           
        }

        

        $jobapplication = new JobsApplications();
        $jobapplication->user_id = Auth::user()->id;
        $jobapplication->job_id = $job->id;
        $jobapplication->status = JobsApplications::NOTVIEWED;
        $jobapplication->coverletter = $request->get('coverletter');
        $jobapplication->levels = JobsApplications::LEVELONE;

        if ($request->hasFile('resume')) {
            $resume = $request->file('resume');
            $name = str_slug(Auth::user()->username."-".Carbon::now()).'.'.$resume->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/resumes');
            $imagePath = $destinationPath. "/".  $name;
            $resume->move($destinationPath, $name);
            $finaldest = '/uploads/resumes/'.$name;
            $jobapplication->resume = $finaldest;
          }
        $jobapplication->save();
        $jobapply= $job->designations->name;
        
        $user->notify(new JobApplied($jobapply));
        return redirect()->route('profile')
        ->withSuccess('You have successfully applied for this job');
    }

    public function showapplicant(JobsApplications $applicant)
    {
        $works =WorkExperience::where('user_id', $applicant->users->id)->get();
        $files = Files::where('user_id',  $applicant->users->id)->get();
        $education = EducationBackground::where('user_id', '=', $applicant->users->id)->get();
        return view('jobs.applicants.show',compact('applicant', 'works', 'education', 'files'));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $job= Job::findOrfail($id);
        $job->delete();
        return redirect()->route('jobs.index')
        ->withSuccess('You have successfully deleted the job Vacancy');
    }

    public function adminapplicantsview()
    {
        $applicants = JobsApplications::all();
        return view('jobs.adminview',compact('applicants'));
    }
}
