<?php

namespace Boaz;

use Illuminate\Database\Eloquent\Model;
use Boaz\User;
use Boaz\Document;

class File extends Model
{
    //
  //   protected $fillable = [
  //   'filename'
  // ];
  protected $fillable = ['user_id','document_id','filename'];

    public function user()
    {
      return $this->belongsTo(User::class);
    }
    
    public function documents()
    {
        return $this->hasOne('Boaz\Document', 'id', 'document_id');
    }
}
